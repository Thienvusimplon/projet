# Mesure d'audience sur le web

[Présentation](https://docs.google.com/presentation/d/1DZKMBnccWDKifPzQFYxaifaVQJG50e82W1UVo9ZovpE/edit?usp=sharing)

# Les questions :

Quel est le traffic sur le site ?  
Combien de personnes visitent le site ?  
Quels sont nos produits les plus et moins intéressants pour notre clientèle ?  
De quel endroit d'internet viennent les visiteurs avant d'arriver sur le site ?  
Quels moments de la journée sont les plus/moins propices à la vente ?  
Quels navigateurs sont les plus / moins utilisés par les visiteurs ?  
Quels systèmes d'exploitation sont les plus / moins utilisés par les visiteurs ?  
Quels appareils sont les plus / moins utilisés par les visiteurs ?  

## Comment répondre à ces questions :

Nous allons automatiser les commandes qui permettent de:

- Télécharger le jeu de donnée (format zip d'origine).
- Décompresser le zipe et garder juste que le fichier qui nous interesse (access.log), et supprimer le reste du contenu du zip.
- Utiliser une regex pour segmenter nos données brutes, qui d'orgine ressemble a une longue liste de ligne.
- Convertir le fichier '.log' en '.csv', ce qui permet de rendre ce dernier compatible avec Sqlite3. 
- Nous utiliserons ensuite metabase qui fonctionne avec Sqlite3 pour lire nos données.  
- Ouvrir Sqlite3 en créant une nouvelle base de donnée '.db'
- Importer le fichier '.csv' dans '.db'
- Fichier .db qui serra importer pour etre lu par metabase.

L'exploitation des données se fera en utilisant l'outil metabase.

## Ces étapes se feront sur 3 scripts :

- pipeline.sh
- logfiletocsv.py
- load.sql

## Expliquation des liens entre les fichiers :

Le script pipeline.sh est le programme de base qui va automatiser les taches. Il va faire appel aux 2 autres fichiers pour effectuer d'autres taches, mais il est le tronc princiaple.

Après avoir téléchargé et gardé juste le fichier access.log contenu dans le dossier zip. Il execute le fichier logfiletocsv.py qui va convertir le fichier '.log'  en fichier '.csv' en utilisant une regex pour segmenter les lignes et les mettre dans des colonnes.

Une fois cette tache faite, le script continue son exécution en ouvrant sqlite3 pour créer une base de donnée. 

Dans sa continuité, il execute le fichier load.sql qui lui se chargera de mettre sqlite3 en mode cvs, et importer le fichier 'propre.csv' dans une nouvelle table que l'on a nommé 'propre'.

### Utilisation du pipeline.sh

```bash
bash pipeline.sh
```
En exécutant cette commande, le script se lance et fait son travail.

#### pipeline.sh - Le fichier principal :

```sh
#!/bin/bash 

DATA_URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"                           
DATA_FILE=Access.log           
DATABASE_FILE=database.db       

##########################################

wget -O ${DATA_FILE} ${DATA_URL}   
unzip ${DATA_FILE}                  
rm -r __MACOSX                   
rm ${DATA_FILE}

echo "Téléchargement du dataset (${DATA_FILE}) (${DATA_URL})" 

python3 logfiletocsv.py                     
sqlite3 ${DATABASE_FILE} ".read load.sql" 

echo "Chargement des données en base ${DATABASE_FILE} (load.sql)" 

sudo cp ${DATABASE_FILE} .docker/metabase/

echo "Déplacer ${DATABASE_FILE} dans Metabase"
```

#### logfiletocsv.py :

```py
#!/usr/bin/env python
# coding: utf-8
# In[2]:

import re
import csv                      
from user_agents import parse   

with open('access.log', 'r') as file:                    
    with open('propre.csv', 'w', newline='') as csvfile:    
        with open('error.txt', 'w', newline='') as error:   
            fieldnames = ['host', 'date', 'heure', 'request', 'url', 'protocol', 'referer', 'useragent', 'browser', 'os', 'device' ]                                
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for ligne in file:
                matching = re.match(r"(?P<host>[.\d]+).(?P<rfc1413ident>\S+).(?P<user>\S+).\[(?P<date>(?P<inutile>\S+\/\S*?:)(?P<heure>\d+)(?P<reste>.*))\].\"(?P<request>\S+).(?P<url>\S+).(?P<protocol>\S+)\".(?P<status>\d+).(?P<size>\d+).(?P<referer>\".*?\").(?P<useragent>\".*?\").(?P<jesaispas2>\".*?\")"
    , ligne)            
                if matching is None:
                    error.write(ligne + "\n")
                else:
                    host, rfc1413ident, user, date, inutile, heure, reste,  request, url, protocol, status, size, referer, useragent, jesaispas = matching.groups()
                    ua_string = useragent
                    user_agent = parse(ua_string)
                    browser = user_agent.browser.family
                    os = user_agent.os.family
                    device = user_agent.device.family
                    writer.writerow({'host':host, 'date':date, 'heure':heure, 'request':request, 'url':url, 'protocol':protocol, 'referer':referer, 'useragent':useragent, 'browser' :browser , 'os' :os, 'device': device})
# In[ ]:
```

#### load.sql :

```sql
.mode csv
.separator ,
.import propre.csv propre
```

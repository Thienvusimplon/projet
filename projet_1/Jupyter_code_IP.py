#!/usr/bin/env python
# coding: utf-8

# In[8]:


import re
import csv
from user_agents import parse

with open('access.log', 'r') as file:
    with open('propre_IP.csv', 'w', newline='') as csvfile:
        with open('error_IP.txt', 'w', newline='') as error:
            fieldnames = ['host']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for ligne in file:
                matching = re.match(r"(?P<host>[.\d]+).(?P<tiret1>\S+).(?P<tiret2>\S+).(?P<date>\[.*\]).(?P<request>\".*?\").(?P<status>\d+).(?P<size>\d+).(?P<page_internet>\".*?\").(?P<appareil>\".*?\").(?P<jesaispas2>\".*?\")", ligne)            
                if matching is None:
                    error.write(ligne + "\n")
                else:
                    host, rfc1413ident, user, date, request, status, size, referer, useragent, jesaispas = matching.groups()                   
                    writer.writerow({'host':host})


# In[ ]:





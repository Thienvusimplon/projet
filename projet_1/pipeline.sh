#!/bin/bash 

DATA_URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"                             # Le lien qui va download ma database
DATA_FILE=Access.log                # Nom donné au fichier téléchargé (avec 'A' majuscule pour différencier du fichier log à l'interieur 'access.log')
DATABASE_FILE=database3.db          # Nom donné à la base de donnée créer

##########################################

wget -O ${DATA_FILE} ${DATA_URL}    # Donne le nom '$x' -> au fichier télécharger '$y'
unzip ${DATA_FILE}                  # Dézip le fichier télécharger
rm -r __MACOSX                      # Ces 2 lignes retire les fichier et dossier inutil du zip
rm ${DATA_FILE}

echo "Téléchargement du dataset (${DATA_FILE}) (${DATA_URL})" 

python3 logfiletocsv.py                     # Execute notre script python
sqlite3 ${DATABASE_FILE} ".read load.sql"   # Ouvre sqlite3 en créant ma database + 

echo "Chargement des données en base ${DATABASE_FILE} (load.sql)" 

sudo cp ${DATABASE_FILE} .docker/metabase/ # Déplace ma database dans metabase

echo "Déplacer ${DATABASE_FILE} dans Metabase"

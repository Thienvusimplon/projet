import urllib.parse
import pandas as pd

def url_change(df):
    '''
    Change the encode of the link in order to make the merge
    '''

    return urllib.parse.unquote(df)

def import_urls():
    '''
    Importing the csv
    '''

    csv1 = '../csv/fusion_comp.csv'
    csv2 = '../csv/fusion_sans_doublons.csv'

    fusion_comp = pd.read_csv(csv1, names = ['url', 'description'])

    fusion_sans_doublons = pd.read_csv(csv2)
    fs = fusion_sans_doublons.copy()

    f_url = fusion_comp['url'].map(url_change)
    fusion_comp['url'] = f_url
    fusion_comp = fusion_comp.drop_duplicates(subset=None, keep='first', inplace=False)

    fc = fusion_comp.copy()

    fusion_merged = fc.merge(fs, left_on='url',
                                 right_on='lien',
                                 how='right',
                                 indicator=True)

    fusion_merged = fusion_merged[['intitule',
                                  'nom_entreprise',
                                  'lieu',
                                  'date',
                                  'lien',
                                  'type_poste',
                                  'description']]

    return fusion_merged

def ranking_competences(option):
    '''
    Contabilisation et calcule de porcentage de mots clés dans les contenus de chaque offre.
    '''

    df = pd.read_csv("../csv/fusion_sans_doublons.csv")
    ptit_outils = [' SQL ',' Python ',' Git ', ' analyse ', ' visualization ',' bi ',
               ' data base ', 'base de données', 'bases de données']

    outils = [' SQL ',' PostgreSQL ',' MySQL ',' Python ',' JSON ',' XML ',' CSV ',' Numpy ',' Panda ',
    ' Matplotlib ',' Psycopg ',' SQLAlchemy ',' SQLite ',' HTML ',' Git ',' GitLab Pages ',' Metabase ',
    ' Bash ',' APIs ',' Scraping ',' Folium ',' Jupyter Notebook ',' analyse ',' visualization ',' bi ',
    ' etl ',' api ',' mining ',' data base ', 'bases de données', 'base de données', ' SCRUM ',' project ',
    ' docker ',' MariaDB ',' PL/SQL ', ' MySQL Workbench ',' Scipy ',' Flask ',' NoSQL ',' MongoDB ',
    ' Robo 3t ',' CouchDB ']

    outils_m = []
    for r in outils:
        outils_m.append(r.lower().strip())

    ptit_outils_m= []
    for r in ptit_outils:
        if 'bi' in r:
            ptit_outils_m.append(r.lower())
        else:
            ptit_outils_m.append(r.lower().strip())

    if option == 'o':
        var = outils_m
    elif option == 'p':
        var = ptit_outils_m

    list_comp = []
    count = 0
    for s in range(df.shape[0]):
        mots = []
        for f in var:
            try:
                if f.lower() in (df.loc[s,'description']).lower():
                    count += 1
                    mots.append(f.lower())
            except:
                pass
        perc = round(100*count/len(var),2)
        list_comp.append((perc, mots, ))
        count = 0
    lcomp = pd.DataFrame(list_comp, columns = ['pourcentage','mots_cle'])
    df['pourcentage'] = lcomp['pourcentage']
    df['mots_cle'] = lcomp['mots_cle']

    df = df.drop(['description'], axis=1)
    df.to_csv("../csv/fusion_sans_doublons.csv", index =False)
    df.to_csv("../data/fusion_sans_doublons.csv", index=False)

    return df

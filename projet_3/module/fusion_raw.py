import os
import pandas as pd
import algo_type_emploi as ate

def fusion_raw():
    """
    Fusionne les 3 csv des offres d'emploi en un seul en y ajoutant le type de poste
    """

    pe = pd.read_csv("../csv/pe.csv")
    monster = pd.read_csv("../csv/monster.csv")
    indeed = pd.read_csv("../csv/indeed.csv")

    if os.path.isfile("../csv/fusion_raw.csv"):
        df = pd.read_csv("../csv/fusion_raw.csv")
        final_df = pd.concat([df, pe, monster, indeed])
    else:
        final_df = pd.concat([pe, monster, indeed])

    final_df["type_poste"] = ""
    var = 0
    for intitule in final_df.intitule:

        final_df["type_poste"][var] = ate.get_type_poste(intitule)
        var += 1

    final_df.to_csv('../csv/fusion_raw.csv', index=False)

def aurevoir_doublons():
    """
    Retire les doublons du csv
    """

    df = pd.read_csv("../csv/fusion_raw.csv")
    df = df.drop_duplicates(subset= ["intitule","nom_entreprise","lieu","date"],keep='first',inplace=False)
    df.to_csv("../csv/fusion_sans_doublons.csv", index=False)

    return df

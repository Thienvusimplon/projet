import psycopg2
import pandas as pd
from psycopg2.extensions import parse_dsn
from sqlalchemy import create_engine

def create_database():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    cur = conn.cursor()
    engine = create_engine(db_dsn)

    emploi_data = pd.read_csv("../csv/fusion_sans_doublons.csv")
    emploi_data.to_sql('emploi_data', engine, if_exists='replace', index = False)

    monster = pd.read_csv("../csv/monster.csv")
    monster.to_sql('monster', engine, if_exists='replace', index = False)

    pe = pd.read_csv("../csv/pe.csv")
    pe.to_sql('pe', engine, if_exists='replace', index = False)

    indeed = pd.read_csv("../csv/indeed.csv")
    indeed.to_sql('indeed', engine, if_exists='replace', index = False)

    conn.commit()
    cur.close()
    conn.close()

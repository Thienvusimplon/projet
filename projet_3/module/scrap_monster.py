import re
from datetime import date, timedelta
import os
import requests
from bs4 import BeautifulSoup
import pandas as pd

def recup_nb_page(*args):
    """
    La fonction recupère le nombre maximum d'offre possible sur le site monster lié à la data
    """

    liste_argument = []

    if os.path.isfile("../csv/fusion_sans_doublons.csv"):
        for argument in args:
            url = f"https://www.monster.fr/emploi/recherche/?q={argument}&where=Rh__C3__B4ne__2DAlpes&cy=fr&rad=20"
            # Recuperation du code source html de la premiere page de Monster lié à la data
            r = requests.get(url, auth=('user', 'pass'))
            #r.status_code
            #r.headers['content-type']
            soup = BeautifulSoup(r.text, 'html.parser')

            # Creation de l'url en fonction du nom max de page
            url2 = f'{url}+&stpage=1&page=1'
            liste_argument.append(url2)
        print("scraping de la première page")
    else:
        for argument in args:
            URL = f"https://www.monster.fr/emploi/recherche/?q={argument}&where=Rh__C3__B4ne__2DAlpes&cy=fr&rad=20"
            # Recuperation du code source html de la premiere page de Monster lié à la data
            r = requests.get(URL, auth=('user', 'pass'))
            #r.status_code
            #r.headers['content-type']
            soup = BeautifulSoup(r.text, 'html.parser')

            # Recupere le nombre maximum d'offre sur Monster sur la region Rhone-Alpes
            a = soup.findAll("div",{"class" : "mux-search-results"})

            # Calcul du nombre maximum de page max offre/offre sur une page
            result = int(int(a[0].get('data-results-total'))/int(a[0].get('data-results-per-page')))

            # Creation de l'url en fonction du nom max de page
            url2 = f'{URL}+&stpage=1&page={result}'
            liste_argument.append(url2)
        print("scraping de toutes les pages")

    return liste_argument

def scrap_monster(url2):
    '''
    La fonction scrap le site monster
    '''

    tableau = {"intitule": [], "nom_entreprise": [], "lieu": [], "date": [], "lien": []}

    # Recuperation du code source html de TOUTES les pages de Monster lié à la data
    for element in url2:
        t = requests.get(element, auth=('user', 'pass'))
        #t.status_code
        #t.headers['content-type']
        soupt = BeautifulSoup(t.text, 'html.parser')

        # Recuperation des balises
        scrap_intitule = soupt.find_all("h2", class_ = "title")

        scrap_lien = soupt.find_all("h2", class_ = "title")
        scrap_nom_entreprise= soupt.find_all("div", class_ = "company")
        scrap_lieu = soupt.find_all("div", class_ = "location")
        scrap_date = soupt.find_all("div", class_ = "meta flex-col")

        # Ajout des informations récupéré dans le dictionnaire
        for i in range(len(soupt.find_all("div", {"class":"summary"}))-1):
            tableau["intitule"].append(scrap_intitule[i].text.replace("\n", "").strip())
            tableau["lien"].append(scrap_lien[i].find('a').get('href'))
            tableau["nom_entreprise"].append(scrap_nom_entreprise[i].text.replace("\n", " ").strip())
            tableau["lieu"].append(scrap_lieu[i+1].text.replace("\n", "").lower().replace(", auvergne-rhône-alpes", " ").replace("vénissieux", "venissieux").strip())
            tableau["date"].append(scrap_date[i].text.replace("\n", "").strip().replace("PostuléSauvegardée", " ").replace("Publiée ", " "))

    # Affichage de la date format AA/MM/JJ à la place de il y a 4 jours
    for i in range(len(tableau["date"])):

        # Utilisation d'une regex pour chercher le numero dans il y 4 jours
        matching = re.match(r"(?P<il_y_a>\D+)(?P<numero>\d+)(?P<text>\D+)", tableau["date"][i])
        if matching:
            il_y_a, numero, text = matching.groups()
        #Si un numero est extrait, on utilise le numero pour le soustraire à la date d'aujorud'hui
            if int(numero) < 30:
                tableau["date"][i] = date.today() - timedelta(days=int(numero))

        #Sur les lignes ou est écrit aujourd'hui, affichage de la date d'aujourd'hui
        else:
            tableau["date"][i] = date.today()

    df = pd.DataFrame(tableau)
    df.to_csv("../csv/monster.csv", index =False)

    return df

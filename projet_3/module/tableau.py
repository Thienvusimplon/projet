import os
import jinja2 as jj
import pandas as pd
from bs4 import BeautifulSoup

def get_tableau():
    df = pd.read_csv("../csv/fusion_sans_doublons.csv")
    template_dir = os.path.join('../templates')
    jinja_env = jj.Environment(loader = jj.FileSystemLoader(template_dir))

    template = jinja_env.get_template('liste_emplois.html')

    html_brut = df.to_html(columns=[
        "intitule",
        "nom_entreprise",
        "lieu",
        "date",
        "lien",
        "type_poste",
        "pourcentage",
        "mots_cle"], render_links=True, index=False)
    html_brut = html_brut.replace('\n','')

    soup = BeautifulSoup(html_brut, 'html.parser')
    table = soup.find_all('tbody')

    page = template.render(tableau=table)
    page = page.replace('\n','')
    page = page.replace(',','')
    page = page.replace('[','')
    page = page.replace(']','')

    with open('../liste.html', 'w') as out:
        out.write(page)

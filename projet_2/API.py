	#!/usr/bin/env python
# coding: utf-8

# #### JUST APIs

# In[3]:


#!/usr/bin/env python
# coding: utf-8

import requests
import json
import csv

### Reference links 
### Horaires
refno2_h = "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson"
refpm10_h = "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson"
refpm25_h = "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_2.geojson"
refo3_h = "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_3.geojson"
refso2_h = "https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_5.geojson"

### Journaliers
refno2_j = "https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_1.geojson"
refo3_j = "https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_2.geojson"
refpm10_j = "https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_4.geojson"
refso2_j = "https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_6.geojson"
refpm25_j = "https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_3.geojson"

### Mensuelle
refno2_m = "https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_1.geojson"
refo3_m = "https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_2.geojson"
refpm10_m = "https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_4.geojson"
refso2_m = "https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_6.geojson"
refpm25_m = "https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_3.geojson"

ind = [refno2_h, refpm10_h, refpm25_h, refo3_h, refso2_h, 
       refno2_j, refpm10_j, refpm25_j, refo3_j, refso2_j,
       refno2_m, refpm10_m, refpm25_m, refo3_m, refso2_m]

##  Response indicators list
req = []
for l in ind:
    req.append(requests.get(l))


# In[4]:


## Create de CSV

with open('raw_data.csv', 'a', newline='') as csvfile:
    fieldnames = ['nom_dept', 'nom_com', 'insee_com', 'nom_station', 'code_station', 'typologie',
                  'influence', 'nom_poll', 'polluant_court', 'id_poll_ue', 'valeur', 'unite', 
                  'metrique', 'date_debut', 'date_fin', 'validite','statut_valid', 'x_wgs84', 
                  'y_wgs84']   

   
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    
    for tail in req:
        lenght = len(tail.json()["features"])
        data = tail.json()["features"]
       
        for i in range(lenght):
            
            nom_dept = data[i]["properties"]["nom_dept"]
            nom_com = data[i]["properties"]["nom_com"]
            insee_com = data[i]["properties"]["insee_com"]
            nom_station = data[i]["properties"]["nom_station"]
            code_station = data[i]["properties"]["code_station"]
            typologie = data[i]["properties"]["typologie"]
            influence = data[i]["properties"]["influence"]
            nom_poll = data[i]["properties"]["nom_poll"]
            
            if "polluant_court" not in data[i]["properties"].keys():
                polluant_court = ''
            else:
                polluant_court = data[i]["properties"]["polluant_court"]

            id_poll_ue = data[i]["properties"]["id_poll_ue"]
            valeur = data[i]["properties"]["valeur"]
            unite = data[i]["properties"]["unite"]
            metrique = data[i]["properties"]["metrique"]
            date_debut = data[i]["properties"]["date_debut"]
            date_fin = data[i]["properties"]["date_fin"]           
            if "validite" not in data[i]["properties"].keys():
                validite = ''
            else:
                validite = data[i]["properties"]["validite"]
            if "statut_valid" not in data[i]["properties"].keys():
                 statut_valid = ''
            else:
                statut_valid = data[i]["properties"]["statut_valid"]              
            x_wgs84 = data[i]["properties"]["x_wgs84"]
            y_wgs84 = data[i]["properties"]["y_wgs84"]
            
            writer.writerow({'nom_dept':nom_dept, 'nom_com':nom_com, 
                            'insee_com':insee_com, 'nom_station':nom_station, 
                            'code_station':code_station, 'typologie':typologie,
                            'influence':influence, 'nom_poll':nom_poll, 
                            'polluant_court':polluant_court, 'id_poll_ue':id_poll_ue, 
                            'valeur':valeur, 'unite':unite, 'metrique':metrique, 
                            'date_debut':date_debut, 'date_fin':date_fin, 
                            'validite':validite,'statut_valid':statut_valid,
                            'x_wgs84':x_wgs84, 'y_wgs84':y_wgs84})      


 


# In[ ]:





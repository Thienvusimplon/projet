import pandas as pd
df = pd.read_csv('raw_data_dedup.csv',dtype={"nom_dept": "string","nom_com": "string",
            "insee_com": "string",
            "nom_station": "string",
            "code_station": "string",
            "typologie": "string",
            "influence": "string",
            "nom_poll": "string",
            "polluant_court" : "string",
            "id_poll_ue": "string",
            "valeur": "string",
            "unite": "string",
            "metrique": "string",
            "date_debut": "string",
            "date_fin": "string",
            "statut_valid": "string",
            "validite": "string",
            "x_wgs84": "string",
            "y_wgs84": "string"})
df=df.drop_duplicates(subset= ['date_debut','x_wgs84','y_wgs84','nom_poll','metrique'],keep='first',inplace=False)
df.to_csv('raw_data_analyse.csv', index=False)

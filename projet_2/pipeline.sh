#lancement du script shell
echo "connection des conteneurs"
sudo docker-compose down
sudo docker-compose up -d
sudo docker-compose ps

date
echo " lancement du pipeline"

echo "script python d'extraction et creation csv"
python3 API.py

echo "suppression des doublons de lignes"
python3 dedoublage.py

echo "suppression des doublons selon les critères d'analyse"
python3 dedoublonanalyse.py

echo "copie des fichiers dans le conteneur"
sudo cp raw_data_analyse.csv .docker/data/

echo "création de la table des données brutes"
python3 TableRAW_DATA.py

echo "création des tables de références"
python3 ModeleAnalytique.py

date
echo "exécution du pipeline terminé"

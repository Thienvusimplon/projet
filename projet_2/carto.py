import folium
import json
import requests
import psycopg2
from psycopg2.extensions import parse_dsn
import pandas as pd

def affichage_map():
	db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
	db_args = parse_dsn(db_dsn)
	conn = psycopg2.connect(**db_args)

	janvier_2020_pm10 = pd.read_sql('''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
		        FROM mesure
		        JOIN polluant on polluant.id_polluant = mesure.reference_polluant
		        JOIN departement on departement.id_departement = mesure.reference_departement
		        WHERE mesure.metrique = 'mensuel' and  polluant.nom_poll = 'particules PM10'and mesure.date_debut >= 
		        timestamp with time zone '2020-01-01 00:00:00.000Z' AND mesure.date_debut < 
		        timestamp with time zone '2020-02-01 00:00:00.000Z'
		        GROUP BY departement.nom_departement, polluant.nom_poll''',conn)
	fevrier_2020_pm10 = pd.read_sql('''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
		        FROM mesure
		        JOIN polluant on polluant.id_polluant = mesure.reference_polluant
		        JOIN departement on departement.id_departement = mesure.reference_departement
		        WHERE mesure.metrique = 'mensuel' and  polluant.nom_poll = 'particules PM10'and mesure.date_debut >= 
		        timestamp with time zone '2020-02-01 00:00:00.000Z' AND mesure.date_debut < 
		        timestamp with time zone '2020-03-01 00:00:00.000Z'
		        GROUP BY departement.nom_departement, polluant.nom_poll''',conn)
	mars_2020_pm10 = pd.read_sql('''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
		        FROM mesure
		        JOIN polluant on polluant.id_polluant = mesure.reference_polluant
		        JOIN departement on departement.id_departement = mesure.reference_departement
		        WHERE mesure.metrique = 'mensuel' and  polluant.nom_poll = 'particules PM10'and mesure.date_debut >= 
		        timestamp with time zone '2020-03-01 00:00:00.000Z' AND mesure.date_debut < 
		        timestamp with time zone '2020-04-01 00:00:00.000Z' AND mesure.valeur IS NOT NULL
		        GROUP BY departement.nom_departement, polluant.nom_poll''',conn)
	avril_2020_pm10 = pd.read_sql('''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
		        FROM mesure
		        JOIN polluant on polluant.id_polluant = mesure.reference_polluant
		        JOIN departement on departement.id_departement = mesure.reference_departement
		        WHERE mesure.metrique = 'mensuel' and  polluant.nom_poll = 'particules PM10'and mesure.date_debut >= 
		        timestamp with time zone '2020-04-01 00:00:00.000Z' AND mesure.date_debut < 
		        timestamp with time zone '2020-05-01 00:00:00.000Z' AND mesure.valeur IS NOT NULL
		        GROUP BY departement.nom_departement, polluant.nom_poll''',conn)
	mai_2020_pm10 = pd.read_sql('''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
		        FROM mesure
		        JOIN polluant on polluant.id_polluant = mesure.reference_polluant
		        JOIN departement on departement.id_departement = mesure.reference_departement
		        WHERE mesure.metrique = 'mensuel' and  polluant.nom_poll = 'particules PM10'and mesure.date_debut >= 
		        timestamp with time zone '2020-05-01 00:00:00.000Z' AND mesure.date_debut < 
		        timestamp with time zone '2020-06-01 00:00:00.000Z' AND mesure.valeur IS NOT NULL
		        GROUP BY departement.nom_departement, polluant.nom_poll''',conn)


	df_janvier = pd.DataFrame(janvier_2020_pm10, columns =['dept', 'moy'])
	df_fevrier = pd.DataFrame(fevrier_2020_pm10, columns =['dept', 'moy'])
	df_mars = pd.DataFrame(mars_2020_pm10, columns =['dept', 'moy'])
	df_avril = pd.DataFrame(avril_2020_pm10, columns =['dept', 'moy'])
	df_mai = pd.DataFrame(mai_2020_pm10, columns =['dept', 'moy'])

	m = folium.Map(location=[45.188529,5.724524], zoom_start=7)
	m.choropleth(
	    geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
	    name ='janvier_2020_pm10',
	    data = janvier_2020_pm10,
	    columns = ['dept','moy'],
	    key_on = 'feature.properties.nom',
	    fill_color = 'YlGn',
	    fill_opacity = 0.7,
	    line_opacity = 0.2,
	    legend_name = 'valeur pm10',
	    bins=[0, 20, 40, 50, 100, 150],
	    highlight=True)
	m.choropleth(
	    geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
	    name ='fevrier_2020_pm10',
	    data = fevrier_2020_pm10,
	    columns = ['dept','moy'],
	    key_on = 'feature.properties.nom',
	    fill_color = 'YlGn',
	    fill_opacity = 0.7,
	    line_opacity = 0.2,
	    legend_name = 'valeur pm10',
	    bins=[0, 20, 40, 50, 100, 150],
	    highlight=True)
	m.choropleth(
	    geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
	    name ='mars_2020_pm10',
	    data = mars_2020_pm10,
	    columns = ['dept','moy'],
	    key_on = 'feature.properties.nom',
	    fill_color = 'YlGn',
	    fill_opacity = 0.7,
	    line_opacity = 0.2,
	    legend_name = 'valeur pm10',
	    bins=[0, 20, 40, 50, 100, 150],
	    highlight=True)
	m.choropleth(
	    geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
	    name ='avril_2020_pm10',
	    data = avril_2020_pm10,
	    columns = ['dept','moy'],
	    key_on = 'feature.properties.nom',
	    fill_color = 'YlGn',
	    fill_opacity = 0.7,
	    line_opacity = 0.2,
	    legend_name = 'valeur pm10',
	    bins=[0, 20, 40, 50, 100, 150],
	    highlight=True)
	m.choropleth(
	    geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
	    name ='mai_2020_pm10',
	    data = mai_2020_pm10,
	    columns = ['dept','moy'],
	    key_on = 'feature.properties.nom',
	    fill_color = 'YlGn',
	    fill_opacity = 0.7,
	    line_opacity = 0.2,
	    legend_name = 'valeur pm10',
	    bins=[0, 20, 40, 50, 100, 150],
	    highlight=True)


	folium.LayerControl().add_to(m)
	return m


print("import du module postgre pour connection")
import psycopg2
from psycopg2.extensions import parse_dsn

db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
db_args = parse_dsn(db_dsn)
conn = psycopg2.connect(**db_args)

cur = conn.cursor()
print("création table département")
cur.execute("""DROP TABLE IF EXISTS departement CASCADE;
CREATE TABLE departement ("id_departement" SERIAL,
    "nom_departement" TEXT,
    PRIMARY KEY("id_departement")
    );
INSERT INTO departement ("nom_departement")
SELECT DISTINCT("nom_dept") FROM raw_data;""")
conn.commit()

print("création table commune")
cur.execute(""" DROP TABLE IF EXISTS commune CASCADE;
    CREATE TABLE commune (
    "id_commune" SERIAL,
    "nom_commune" TEXT,
    "insee_com" TEXT,
    PRIMARY KEY("id_commune")
    );
INSERT INTO commune(nom_commune,insee_com)
SELECT DISTINCT(nom_com),insee_com 
FROM raw_data
;""")
conn.commit()

print("création table station")
cur.execute("""DROP TABLE IF EXISTS station CASCADE;
    CREATE TABLE station 
    (
    "id_station" SERIAL,
    "nom_station" TEXT,
    "longitude" FLOAT,
    "latitude" FLOAT,
    PRIMARY KEY("id_station")
    );
INSERT INTO station(nom_station,longitude, latitude)
SELECT DISTINCT(nom_station), x_wgs84, y_wgs84 
FROM raw_data
;""")
conn.commit()

print("création table polluant")
cur.execute("""DROP TABLE IF EXISTS polluant CASCADE;
    CREATE TABLE polluant 
    (
    "id_polluant" SERIAL,
    "nom_poll" TEXT,
    PRIMARY KEY("id_polluant")
    );
INSERT INTO polluant(nom_poll)
SELECT DISTINCT(nom_poll) 
FROM raw_data;""")
conn.commit()

print("création table mesure")
cur.execute("""DROP TABLE IF EXISTS mesure CASCADE;
    CREATE TABLE mesure 
    (
    "id_mesure" SERIAL,
    "valeur" NUMERIC NULL,
    "reference_station" INT,
    "reference_polluant" INT,
    "reference_commune" INT,
    "reference_departement" INT,
    "date_debut" TIMESTAMP WITH TIME ZONE,
    "date_fin" TIMESTAMP WITH TIME ZONE,
    "unite" TEXT,
    "metrique" TEXT,
    PRIMARY KEY("id_mesure"),
    FOREIGN KEY ("reference_station") REFERENCES station ("id_station"),
    FOREIGN KEY ("reference_polluant") REFERENCES polluant ("id_polluant"),
    FOREIGN KEY ("reference_commune") REFERENCES commune ("id_commune"),
    FOREIGN KEY ("reference_departement") REFERENCES departement ("id_departement")
    );
INSERT INTO mesure("valeur","reference_station","reference_polluant","reference_commune","reference_departement","date_debut","date_fin","unite",
    "metrique")
SELECT valeur,station.id_station,polluant.id_polluant,commune.id_commune,departement.id_departement,date_debut,date_fin,unite,metrique 
FROM raw_data
JOIN station
ON station.nom_station = raw_data.nom_station
JOIN polluant
ON polluant.nom_poll = raw_data.nom_poll
JOIN commune
ON commune.nom_commune=raw_data.nom_com
JOIN departement
ON departement.nom_departement=raw_data.nom_dept
;""")
conn.commit()
print("création des tables du modèle analytique terminée!")
cur.close()
conn.close()

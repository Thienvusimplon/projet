import psycopg2
from psycopg2.extensions import parse_dsn

def requete_sql():
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)


    cur = conn.cursor()
    test = input("Entrez votre requete:")

    while test != "stop":
        cur.execute(test)
        all_traffic = cur.fetchall()
        print(all_traffic)
        test = input("Entrez votre requete:")

    cur.close()
    conn.close()

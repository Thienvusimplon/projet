import folium
import json
import requests
import psycopg2
from psycopg2.extensions import parse_dsn
import pandas as pd



def affichage_map(premier_mois,annee,polluant,legend,m):
    db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)
    
    deuxieme_mois = premier_mois
    deuxieme_annee = annee
    if deuxieme_mois == '12':
        deuxieme_mois = '00'
        deuxieme_annee = int(annee) + 1
    if polluant == 'pm10':
        polluant = 'particules PM10'
        scale = [0, 10, 20, 30, 40, 50, 60]
        couleur = 'Blues'
    if polluant == 'pm25':
        polluant = 'particules PM2,5'
        scale = [0, 10, 20, 30, 40, 50]
        couleur = 'Reds'
    if polluant == 'o3':
        polluant = 'ozone'
        scale = [0, 15, 30, 45, 60, 75, 90, 105]
        couleur = 'Greens'
    if polluant == 'so2':
        polluant = 'dioxyde de soufre'
        scale = [0, 10, 20, 30, 40, 50]
        couleur = 'Greys'
    if polluant == 'no2':
        polluant = 'dioxyde d''azote'
        scale = [0, 10, 20, 30, 40, 50, 60, 70]
        couleur = 'Purples'
        
    requete = pd.read_sql(f'''SELECT distinct(departement.nom_departement) as dept, avg(mesure.valeur) as moy
                FROM mesure
                JOIN polluant on polluant.id_polluant = mesure.reference_polluant
                JOIN departement on departement.id_departement = mesure.reference_departement
                WHERE mesure.metrique = 'journalier' and  polluant.nom_poll = '{polluant}'and mesure.date_debut >= 
                timestamp with time zone '{annee}-{premier_mois}-01 00:00:00.000Z' AND mesure.date_debut < 
                timestamp with time zone '{deuxieme_annee}-{int(deuxieme_mois)+1}-01 00:00:00.000Z' AND mesure.valeur > 0
                AND mesure.valeur IS NOT NULL
                GROUP BY departement.nom_departement, polluant.nom_poll''',conn)    
    
    
    choropleth = folium.Choropleth(
        geo_data = "https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson",
        name =f"{premier_mois}_{deuxieme_annee}_{polluant}",
        data = requete,
        columns = ['dept','moy'],
        key_on = 'feature.properties.nom',
        fill_color = couleur,
        fill_opacity = 0.7,
        line_opacity = 0.2,
        legend_name = f"{premier_mois}_{deuxieme_annee}_{polluant}",
        bins = scale,
        highlight=True).add_to(m)
    if legend == 'off':
        for key in choropleth._children:
            if key.startswith('color_map'):
                del(choropleth._children[key])
       
    return m
    

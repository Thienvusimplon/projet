#!/usr/bin/env python
# coding: utf-8

# In[4]:


import psycopg2
from psycopg2.extensions import parse_dsn

db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
db_args = parse_dsn(db_dsn)
conn = psycopg2.connect(**db_args)

cur = conn.cursor()

cur.execute("""DROP TABLE IF EXISTS raw_data;
CREATE TABLE raw_data (nom_dept TEXT,
            nom_com TEXT,
            insee_com TEXT,
            nom_station TEXT,
            code_station TEXT,
            typologie TEXT,
            influence TEXT,
            nom_poll TEXT,
            polluant_court TEXT,
            id_poll_ue TEXT,
            valeur NUMERIC NULL,
            unite TEXT,
            metrique TEXT,
            date_debut TIMESTAMP WITH TIME ZONE,
            date_fin TIMESTAMP WITH TIME ZONE,
            validite BOOLEAN,
            statut_valid BOOLEAN,            
            x_wgs84 FLOAT,
            y_wgs84 FLOAT);""")
            
conn.commit()
cur.execute("""COPY raw_data FROM '/data/raw_data_analyse.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);""")
conn.commit()

cur.close()
conn.close()


# In[ ]:





# In[ ]:





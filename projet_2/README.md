# Projet 2-Groupe 6

Membres du groupe 6 : Gabriel, Helena, Thienvu

## Pré-requis

docker  
docker-compose  
postgres  
pip3 (module pandas, folium, json, requests, psycopg2, csv)  

## Comment lancer le pipeline

L'articulation du pipeline se fait sur le schéma suivant:


![<Schéma du pipeline>](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/projets/projet_2/projet_2-groupe_6/uploads/9111531b4ea035a8d1e50bf5ef74a8b7/Sch%C3%A9maPipeline.png)

Le script pipeline.sh lance le pipeline:
- le script "API.py" se charge d'extraire les données depuis le site atmo. Les données mensuelles, journalières et horaires qui étaient au format Json sont également transformées au format CSV. Il en ressort le fichier "raw_data.csv".
- le script "dedoublage.py" se charge de supprimer les lignes dupliquées lors de la planification journalière de l'extraction, il en ressort le fichier correspondant "raw_data_dedup.csv".
- le script "dedoublonanalyse.py" se charge de supprimer les données considérees comme doublons dans le cadre de l'analyse; les données sont considérées comme des doublons si elles ont la même date de début, les mêmes coordonnées, le même nom de polluant et la même métrique. A l'issue du script, on obtient le fichiet "raw_data_analyse.csv"
- le script "TableRAW_DATA.py" se charge d'importer le fichier "raw_data_analyse.csv" dans la base de données PostGres.
- le script ModèleAnalytique.py se charge de créer les tables de références qui caractérisent les données et la table "mesure" qui regroupe les métriques.

![<Schéma du modèle de base de données>](https://gitlab.com/simplon-dev-data/grenoble-2020-2021/projets/projet_2/projet_2-groupe_6/uploads/767f27e49c9db0472f594dabb8f473ca/Sch%C3%A9maMod%C3%A8leAnalytique.png)

A l'issue du script, vous serez en mesure de visualiser vos donées dans metabase et d'exploiter vos données dans Jupyter notebook.


## Bibliographie et liens externes

Lien de la présentation :
https://docs.google.com/presentation/d/1Y5v0zQOdthk1087pwyIXkCa3fZsXgLM6nHY4PwKzyvY/edit?usp=sharing

Le site atmo où récupérer les données:
https://data-atmoaura.opendata.arcgis.com/

Explication sur les indicateurs de pollution et les particules:
https://www.r-pur.com/blogs/air/comprendre-facilement-la-pollution-air-les-particules-fines-pm-2-5?utm_source=google&utm_medium=cpc&gclid=CjwKCAiAl4WABhAJEiwATUnEFx5kXE3HuFIqsnh_RUY5U46wPtv1MmWx9KsZhqGTQ3Rn42XPe1qa3xoC7E4QAvD_BwE

Carte de france avec frontières des régions et départements au format JSON:    
https://france-geojson.gregoiredavid.fr/repo/regions/auvergne-rhone-alpes/departements-auvergne-rhone-alpes.geojson
https://france-geojson.gregoiredavid.fr/

Sur le modèle transactionnel et relationnel:
https://maximilienandile.github.io/2016/10/12/Base-de-donnees-comprendre-les-differences-entre-le-modele-relationnel-et-le-modele-multidimensionnel/

Des explications sur l'utilisation de folium:
https://fxjollois.github.io/cours-2016-2017/analyse-donnees-massives-tp9.html
https://www.youtube.com/watch?list=LL&v=4RnU5qKTfYY&feature=youtu.be
